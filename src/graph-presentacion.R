# Presentación Madison
# 
# Author: Alicia Franco
# Date:25/03/2021
# 
#### Paquetería ####
require(pacman)
p_load(tidyverse, here, data.table, janitor, readxl)

#### Directorios ####
inp <- list(img_out = here("out/img/"),
            data_inp = here("inp/data/"),
            data_out = here("out/data/"),
            theme = here("src/tema_propio.R"))

top10 <- c("Mexico", "Peru","Netherlands", "Spain", "Chile", "Kenya", "Colombia", "United States of America", "Dominican Republic", "South Africa")
colors <- c("#4dac89", "#2f1b2f", "#6f7e9d", "#23576d", "#122038", "#e1ecee")

# Agri I was right
agri <- bind_rows(map(agri_ent_files, import_agri_ent)) 

tempo <- agri %>% 
  mutate(tot = sum(porc_ent_anual)) %>% 
  group_by(nomestado) %>% 
  summarise(porc = 100*sum(porc_ent_anual)/tot) %>%
  distinct()


tempo <- agri %>% 
  group_by(year, nomestado) %>% 
  summarise(tot = sum(porc_ent_anual)/10000) %>% 
  ungroup() %>% 
  filter(nomestado == "Michoacán")
#### Importemos ####
exp_glob <- read.csv(paste0(inp$data_inp, "FAOSTAT_data_3-25-2022.csv")) %>%
  clean_names() %>% 
  select(area,element,year,unit, value) %>% 
  filter(element == "Export Quantity") %>% 
  group_by(area,year) %>% 
  summarise(value = sum(value, na.rm = T)/10000) %>% 
  ungroup() %>% 
  filter(area %in% top10) %>% 
  mutate(area = case_when(area == "United States of America" ~ "EE.UU", 
                          area == "Peru" ~ "Perú",
                          area == "Netherlands" ~ "Holanda",
                          area == "Spain" ~ "España",
                          area == "Mexico" ~ "México",
                          T ~ area),
         color = factor(as.integer(area == "México")))

ggplot() +
  geom_line(data = exp_glob, aes(x = as.integer(year), y = value, group = area, color = color), alpha = 0.7, size = 1) +
  geom_point(data = exp_glob, aes(x = as.integer(year), y = value, group = area, color = color), size = 2) +
  geom_line(data = tempo, aes(x = as.integer(year), y = tot), alpha = 0.7, size = 1, color = pal_mal[3]) +
  geom_point(data = tempo, aes(x = as.integer(year), y = tot), size = 2, color = pal_mal[3]) +
  geom_text(data = exp_glob %>% filter(year == 2020, value>6.7, !(area %in% c("Kenya", "Colombia"))), 
            aes(y = value, x = 2020.5, label = area, color = color), fontface = "bold") +
  geom_text(data = tempo %>% filter(year == 2020), 
            aes(y = tot -10 , x = 2020.5),
            label = "Toneladas\nproducidas\nen Michoacán",
            fontface = "bold", 
            color = pal_mal[3]) +
  labs(y = "10,000 toneladas de Aguacate exportadas", 
       x = "",
       title = "Toneladas de Aguacate exportadas",
       caption = "Fuente: Elaboración propia a partir de datos de la FAO") +
  scale_color_manual(values = c(pal_mal[2], pal_mal[1]))+
  scale_x_continuous(breaks = 2010:2020) +
  tema +
  theme(legend.position = "none") 

ggsave(paste0(inp$img_out, "expandmich.png"), width = 10, height = 6)

ggplot() +
  geom_line(data = exp_glob, aes(x = as.integer(year), y = value, group = area, color = color), alpha = 0.7, size = 1) +
  geom_point(data = exp_glob, aes(x = as.integer(year), y = value, group = area, color = color), size = 2) +
  geom_text(data = exp_glob %>% filter(year == 2020, value>6.7, !(area %in% c("Kenya", "Colombia"))), 
            aes(y = value, x = 2020.5, label = area, color = color), fontface = "bold") +
  labs(y = "10,000 toneladas de Aguacate exportadas", 
       x = "",
       title = "Toneladas de Aguacate exportadas",
       caption = "Fuente: Elaboración propia a partir de datos de la FAO") +
  scale_color_manual(values = c(pal_mal[3], pal_mal[1]))+
  scale_x_continuous(breaks = 2010:2020) +
  tema +
  theme(legend.position = "none") 

ggsave(paste0(inp$img_out, "exp.png"), width = 10, height = 8)
  
  
agri_ent_files <- dir(paste0(inp$data_inp, "cierre_agrícola_SIAP"))

import_agri_ent <- function(file){
  tempo <- fread(paste0(inp$data_inp, "cierre_agrícola_SIAP/",file),encoding = "Latin-1") %>% 
    clean_names() %>% 
    select(year = anio, nomestado, idcultivo, volumenproduccion) %>% 
    filter(idcultivo == 5060000) %>% 
    group_by(year, nomestado) %>% 
    summarise(porc_ent_anual = sum(volumenproduccion)) %>% 
    ungroup() %>% 
    distinct()
}


  
# Divisas exports
exp_mx <- dir(paste0(inp$data_inp, "exports"))

bienes <- read_xlsx(paste0(inp$data_inp, "exports/",exp_mx[1])), skip = 17) %>% 
  clean_names() %>% 
  setNames(c("fecha", "valor")) %>%
  mutate(bien = "Aguacate") %>% 
  rbind(a <- read_xlsx(paste0(inp$data_inp, "exports/",exp_mx[2])), skip = 17) %>% 
              clean_names() %>% 
              setNames(c("fecha", "valor")) %>% 
              mutate(bien = "Crudo"))

ggplot(bienes) +
  geom_line(aes(x = fecha, y = valor, color = bien))
